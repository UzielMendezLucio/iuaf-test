var collectInfo = null;
var nameFull = null;
var internalRegistration = null;
var autorization = null;
var paymentAmount = null;
var payment = null;
var payConcept = null;
var proofPayment = null;

$(document).ready(function() {
    var forms = document.getElementsByClassName("needs-validation");
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener(
            "submit",
            function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add("was-validated");
            },
            false
        );
    });

    $("#clearButton").click(clear);
    $("#finish").click(agreeAccount);

    //FORMULARIO 1
    //Boton Siguiente
    $("#next-1").click(function() {
        if ($("#fullName").val() == "") {
            alert("Falta el nombre");
            return false;
        } else {
            if ($("#registrationInternal").val() == "") {
                alert("Falta la matricula interna");
                return false;
            } else {
                if ($("#autorizationNumber").val() == "") {
                    alert("Falta el numero de autorizacion");
                    return false;
                } else {
                    $("#step-1")
                        .fadeOut()
                        .addClass("d-none");
                    $("#step-2")
                        .fadeIn()
                        .removeClass("d-none");
                    $("#step2").addClass("active");
                }
            }
        }
    });

    //FORMULARIO 2
    //Boton atras
    $("#back-1").click(function() {
        $("#step-2")
            .fadeOut()
            .addClass("d-none");
        $("#step-1")
            .fadeIn()
            .removeClass("d-none");
        $("#step2").removeClass("active");
    });
    //Boton siguiente
    $("#next-2").click(function() {
        if ($("#payAmount").val() == "") {
            console.log("Falta el campo monto de pago");
            return false;
        } else {
            if ($("#methodPayment").val() == "") {
                console.log("Falta el metodo de pago");
                return false;
            } else {
                if ($("#conceptPay").val() == "") {
                    console.log("Falta el concepto de pago");
                    return false;
                } else {
                    if ($("#paymentProof").val() == "") {
                        console.log("Falta el concepto de pago");
                        return false;
                    } else {
                        $("#step-2")
                            .fadeOut()
                            .addClass("d-none");
                        $("#step-3")
                            .fadeIn()
                            .removeClass("d-none");
                        $("#step3").addClass("active");
                    }
                }
            }
        }
    });

    //FORMULARIO 3
    //Boton atras
    $("#back-2").click(function() {
        $("#step-3")
            .fadeOut()
            .addClass("d-none");
        $("#step-2")
            .fadeIn()
            .removeClass("d-none");
        $("#step3").removeClass("active");
    });

    $("#searchNameLabel").hide();
    $("#searchFilterNames").hide();
    $("#filterFromLabel").hide();
    $("#filterFrom").hide();
    $("#filterToLabel").hide();
    $("#filterTo").hide();
    $("#filterAutorizationLabel").hide();
    $("#filterAutorization").hide();
    $("#filterSearch").hide();
    $('#filterClearButton').hide();

    if ($('#searchFilterNames').val() != '') {
      document.getElementById("options").value = 'name';
        // $('#options').val('name');
        $("#searchNameLabel").show();
        $("#searchFilterNames").show();
        $("#filterFromLabel").hide();
        $("#filterFrom").hide();
        $("#filterToLabel").hide();
        $("#filterTo").hide();
        $("#filterAutorizationLabel").hide();
        $("#filterAutorization").hide();
        $("#filterSearch").show();
        $('#filterClearButton').show();
    }else if ($('#filterFrom').val() != '' && $('#filterTo').val() != '') {
      // $('#options').val('Fecha');
      document.getElementById("options").value = 'date';
      $("#searchNameLabel").hide();
      $("#searchFilterNames").hide();
      $("#filterFromLabel").show();
      $("#filterFrom").show();
      $("#filterToLabel").show();
      $("#filterTo").show();
      $("#filterAutorizationLabel").hide();
      $("#filterAutorization").hide();
      $("#filterSearch").show();
      $('#filterClearButton').show();
  }else if ($('#filterAutorization').val() != '') {
    document.getElementById("options").value = 'auto';
    // $('#options').val('Autorización');
    $("#searchNameLabel").show();
    $("#searchFilterNames").show();
    $("#filterFromLabel").hide();
    $("#filterFrom").hide();
    $("#filterToLabel").hide();
    $("#filterTo").hide();
    $("#filterAutorizationLabel").hide();
    $("#filterAutorization").hide();
    $("#filterSearch").show();
    $('#filterClearButton').show();
}

    $("#filterClearButton").click(clearName);
});

function clear() {
    $("#searchLibrary").val("");
    $("#sendPetititon").submit();
}

function clearName() {
    $("#searchFilterNames").val('');
    $('#filterFrom').val('');
    $('#filterTo').val('');
    $('#filterAutorization').val('')

    $("#optionsFilter").submit();
}

function HandleValue(type, value) {
    $(`#${type}`).val(value);
    console.log(value);
}

//Validar el campo, si pone una letra, lo borra
function validNumbers(variable) {
    Numer = parseInt(variable);
    if (isNaN(Numer)) {
        return "";
    }
    return Numer;
}
function ValNumber(Control) {
    Control.value = validNumbers(Control.value);
}

//Filtro
var option = null;
function show(id) {
    if (id == "name") {
        $("#searchNameLabel").show();
        $("#searchFilterNames").show();
        $("#filterFromLabel").hide();
        $("#filterFrom").hide();
        $("#filterToLabel").hide();
        $("#filterTo").hide();
        $("#filterAutorizationLabel").hide();
        $("#filterAutorization").hide();
        $("#filterSearch").show();
        option = "name";
    }
    if (id == "date") {
        $("#searchNameLabel").hide();
        $("#searchFilterNames").hide();
        $("#filterFromLabel").show();
        $("#filterFrom").show();
        $("#filterToLabel").show();
        $("#filterTo").show();
        $("#filterAutorizationLabel").hide();
        $("#filterAutorization").hide();
        $("#filterSearch").show();
        option = "date";
    }
    if (id == "auto") {
        $("#searchNameLabel").hide();
        $("#searchFilterNames").hide();
        $("#filterFromLabel").hide();
        $("#filterFrom").hide();
        $("#filterToLabel").hide();
        $("#filterTo").hide();
        $("#filterAutorizationLabel").show();
        $("#filterAutorization").show();
        $("#filterSearch").show();
        option = "auto";
    }
}

// Mostrar los datos introducidos en la recopilación de datos
function passValue(prox) {
    // var getValue = prox.value;

    var varName = document.getElementById("fullName").value;
    document.getElementById("nameF").value = varName;
    // console.log(varName);

    var varRegistrationInternal = document.getElementById(
        "registrationInternal"
    ).value;
    document.getElementById("internalR").value = varRegistrationInternal;
    // console.log(varRegistrationInternal);

    var varAutorizationNumber = document.getElementById("autorizationNumber")
        .value;
    document.getElementById("noAutorization").value = varAutorizationNumber;
    // console.log(varAutorizationNumber);

    var varPayAmount = document.getElementById("payAmount").value;
    document.getElementById("paymentA").value = varPayAmount;
    // console.log(varPayAmount);

    var varMethodPayment = document.getElementById("methodPayment").value;
    document.getElementById("paymentM").value = varMethodPayment;
    // console.log(varMethodPayment);

    var varConceptPay = document.getElementById("conceptPay").value;
    document.getElementById("payC").value = varConceptPay;
    // console.log(varConceptPay);

    var varPaymentProof = document.getElementById("paymentProof").value;
    document.getElementById("proofPay").value = varPaymentProof;
    // console.log(varPaymentProof);
}

function filter() {
    if (option == "name") {
        var varFilterName = $("#searchFilterNames").val();
        console.log(varFilterName);
        // $('#filterSearch').css('display', 'none');
    }
    if (option == "date") {
        var varFilterFrom = $("#filterFrom").val();
        console.log(varFilterFrom);
        var varFilterTo = $("#filterTo").val();
        console.log(varFilterTo);
        // $('#filterSearch').css('display', 'none');
    }
    if (option == "auto") {
        var varFilterAutorization = $("#filterAutorization").val();
        console.log(varFilterAutorization);
        // $('#filterSearch').css('display', 'none');
    }
}

function agreeAccount() {
    const format2 = "YYYY-MM-DD";
    var date2 = new Date();
    var formatDays = moment(date2).format(format2);

    $.ajax({
        // url: '/entrySearchMaterial/' + inputSearch,
        url: "accounts",
        method: "POST",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            nameComplete: document.getElementById("nameF").value,
            registrationUser: document.getElementById("internalR").value,
            noAutorization: document.getElementById("noAutorization").value,
            paymentAmount: document.getElementById("paymentA").value,
            paymentMethod: document.getElementById("paymentM").value,
            proofOfPayment: document.getElementById("proofPay").value,
            payConcept: document.getElementById("payC").value,
            payDay: formatDays
        },
        success: function(result) {
            console.log("se agregó correctamente");
        }
    });
}

function downloadExcel() {
    var getOption = document.getElementById("options").value;
    var params = null;
    var optionParams = null;
    var optionSelected = null;

    switch (getOption) {
        case "name":
            params = document.getElementById("searchFilterNames").value;
            optionSelected = getOption;
            break;

        case "date":
            params = document.getElementById("filterFrom").value;
            optionParams = document.getElementById("filterTo").value;
            optionSelected = getOption;

            break;
        case "auto":
            params = document.getElementById("filterAutorization").value;
            optionSelected = getOption;

            break;
        default:
            params = null;
            getOption = "all";
            break;
    }
    window.location.href = `accounts/export/${getOption}/params/${params}/secondParms/${optionParams}`;
}
