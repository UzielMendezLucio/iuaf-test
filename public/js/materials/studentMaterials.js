$(document).ready(function () {

	$("#quarter").hide();
	$("#matter").hide();
	$("#loadingSpinner").hide();
	$("#loadingSpinner2").hide();

	$("#cancelGroup").click(function () {
		$("#nameGroup").val("");
		$("#selectProgram").val("");
		$("#selectQuarter").val("");
		$("#selectMatter").val("");
		$("#quarter").hide();
		$("#matter").hide();
	});

	$('#clearGroup').click(clear);

});

function nameGroups() {
	var nameGroup = null;
	nameGroup = $("#nameGroup").val();
	console.log(nameGroup);
}

function selectPrograms(id) {
	if (id == "") {
		$("#quarter").hide();
		$("#matter").hide();

		cleanVal();
	}
	if (id == "1") {
		loadSpinner();
		cleanVal();
		for (var i = 1; i <= 9; i++) {
			$("#selectQuarter").append(
				"<option value=" + i + ">" + i + "</option>"
			);
		}
	}
	if (id == "2" || id == "3") {
		loadSpinner();
		cleanVal();
		for (var i = 1; i <= 5; i++) {
			$("#selectQuarter").append(
				"<option value=" + i + ">" + i + "</option>"
			);
		}
	}
	if (id == "4") {
		loadSpinner();
		cleanVal();
		for (var i = 1; i <= 2; i++) {
			$("#selectQuarter").append(
				"<option value=" + i + ">" + i + "</option>"
			);
		}
	}
	if (id == "5") {
		loadSpinner();
		cleanVal();
		for (var i = 1; i <= 1; i++) {
			$("#selectQuarter").append(
				"<option value=" + i + ">" + i + "</option>"
			);
		}
	}
	if (id == "6") {
		loadSpinner();
		cleanVal();
		for (var i = 1; i <= 1; i++) {
			$("#selectQuarter").append(
				"<option value=" + i + ">" + i + "</option>"
			);
		}
	}
	var programs = $("#selectProgram").val();
	console.log(programs);
}

function loadSpinner() {
	$("#loadingSpinner").show();
	$("#quarter").hide();
	$("#matter").hide();
	setTimeout(function () {
		$("#quarter").show();
		$("#loadingSpinner").hide();
	}, 1000);
}

function cleanVal() {
	for (var i = 1; i <= 9; i++) {
		$("#selectQuarter option[value=" + i + "]").remove();
	}
}

function selectQuarters(id) {
	loadingSpinner();
	var quarter = $("#selectQuarter").val();
	console.log(quarter);
}

function loadingSpinner() {
	$("#loadingSpinner2").show();
	setTimeout(function () {
		$("#matter").show();
		$("#loadingSpinner2").hide();
	}, 1000);
}

function selectMatters() {
	var matter = $("#selectMatter").val();
	console.log(matter);
	console.log(typeof (matter));
}

function clear() {
	$('#searchGroup').val("");
	$('.search').submit();
}

function deleteGroup(id) {
	Swal.fire({
		title: '¿Deseas eliminar el grupo?',
		icon: 'question',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Si, eliminar',
		cancelButtonText: 'Cancelar'
	}).then((result) => {
		if (result.isConfirmed) {
			$.ajax({

				url: 'groups/disabled/' + id,
				method: "PATCH",
				headers: {
					"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
				},
				data: {
					groupId: id
				},
				success: function (data) {
					if (data == 1) {
						Swal.fire(
							'¡Grupo eliminado!',
							'El grupo ha sido eliminado correctamente',
							'success'
						);
						setTimeout(() => {
							return window.location.href = `groups`;
						}, 500)
					}
				}
			});
		}

	})
}
function editGroup(id) {

			$.ajax({

				url: `groups/${id}/edit` ,
				method: "GET",
				headers: {
					"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
				},
				data: {
					groupId: id
				},
				success: function (data) {
					if (data != null || data != undefined) {
					//Ocultar el loader
					}
				}
			});
}