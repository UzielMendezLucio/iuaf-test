$(document).ready(function() {
    // $('#semesterSelect').hide();   

    $('.cancelSubject').click(function() {
        $('#semester').val('');
        $('#program').val('0');
        $('#nameSubject').val('');
    });

    $('#clearSubject').click(clearSubjects);

    $('#loadingEdit').hide();
});

function clearSubjects() {
    $('#searchSubject').val("");
    $('.formSearchSubject').submit();
}

function subject() {
    var nameSubject = $('#nameSubject').val();
    console.log(nameSubject);
    var editNameSubject = $('#editNameSubject').val();
    console.log(editNameSubject);
}

function programs(id) {

    if (id == "") {
        $("#semesterSelect").hide();
        clean();
    }
    if (id == "1") {
        clean();
        for (var i = 1; i <= 9; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "2" || id == "3") {
        clean();
        for (var i = 1; i <= 5; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "4") {
        clean();
        for (var i = 1; i <= 2; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "5") {
        clean();
        for (var i = 1; i <= 1; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }
    if (id == "6") {
        clean();
        for (var i = 1; i <= 1; i++) {
            $("#semester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
            $("#editSemester").append(
                "<option value=" + i + ">" + i + "</option>"
            );
        }
    }

    var programSelected = $('#program').val();
    console.log(programSelected);
    var EditProgramSelected = $('#editProgram').val();
    console.log(editProgramSelected);
}

function selectSemester() {
    var selectSemester = $('#semester').val();
    console.log(selectSemester);
    var editSelectSemester = $('#editSemester').val();
    console.log(editSelectSemester);
}

function clean() {
    for (var i = 1; i <= 9; i++) {
        $("#semester option[value=" + i + "]").remove();
        $("#editSemester option[value=" + i + "]").remove();
    }
}

function deleteSubject(id) {
    Swal.fire({
        title: '¿Deseas eliminar la materia?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: 'bankMatters/disabled/' + id,
                method: "PATCH",
                headers: {
                    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
                },
                data: {
                    subjectId: id
                },
                success: function(data) {
                    if (data == 1) {
                        Swal.fire(
                            '¡Grupo eliminado!',
                            'La materia ha sido eliminada correctamente',
                            'success'
                        );
                        setTimeout(() => {
                            return window.location.href = `bankMatters`;
                        }, 500)
                    }
                }
            });
        }
    })
}




function loaderEdit(id) {
    $('#loadingEdit').show();
    $('.modal-content').addClass('transparent');
    $('#modal-body').hide();
    $.ajax({

        url: `bankMatters/${id}/edit` ,
        method: "GET",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        data: {
            subjectId: id
        },
        success: function (data) {
            if (data != null || data != undefined) {
            //Ocultar el loader
            $('#loadingEdit').hide();
            $('.modal-content').removeClass('transparent');
            $('#modal-body').show();
            }
        }
    });
}