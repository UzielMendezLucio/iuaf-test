@extends('layouts.dashboard')

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/Home/home.js') }}" ></script> 
@endsection


@section('home')
    <div class="EducativeOffer">
        <div class="cards">
            <div class="degree">
                <a href="#">
                    <p class="title">Licenciatura <br> en derecho</p>
                    <p class="rvoe">RVOE: 201915LD</p>
                </a>
            </div>
        </div>

        <div class="cards">
            <div class="degree-day">
                <a href="#">
                    <p class="title">Jornada de titulación</p>
                    <p class="rvoe">RVOE: 201915LD</p>
                </a>
                
            </div>
        </div>

        <div class="cards">
            <div class="mastery">
                <a href="#">
                    <p class="title">Maestría <br> en derecho</p>
                    <p class="rvoe">RVOE: 201946MD</p>
                </a>
            </div>
        </div>

        <div class="cards">
            <div class="doctorate">
                <a href="#">
                <p class="title">Doctorado <br> en derecho</p>
                <p class="rvoe">RVOE: 201879DD</p>
                </a>
            </div>
        </div>
        {{--  <div class="cards">
            <div class="doctorate">
                <a href="#">
                <p class="title">Doctorado <br> en derecho</p>
                <p class="rvoe">RVOE: 201879DD</p>
                </a>
            </div>
        </div>
        <div class="cards">
            <div class="doctorate">
                <a href="#">
                <p class="title">Doctorado <br> en derecho</p>
                <p class="rvoe">RVOE: 201879DD</p>
                </a>
            </div>
        </div>  --}}
    </div>
@endsection
