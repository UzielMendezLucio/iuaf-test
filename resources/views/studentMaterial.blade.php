@extends('layouts.dashboard')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="{{ asset('js/Home/home.js') }}"></script>
    <script src="{{ asset('js/Library/filterLibrary.js') }}"></script>
    <script src="{{ asset('js/materials/studentMaterials.js') }}"></script>
    @include('popper::assets')
@endsection

@section('home')

    <div id="studentMaterial" class="library">
        <div id="buttons">
            <form class="search">
                <input value="{{ $filter }}" name="find" id='searchGroup' type="text" class="form-control"
                    placeholder="Escribe el nombre de la clase...">
                <button id="sendGroup" type="submit" class="btn"><i class="fas fa-search"></i></button>
                @if ($showClear)
                    <button id="clearGroup" type="button" class="btn btnSearch"><i class="fas fa-trash-alt"></i></button>
                @endif
            </form>

            <button id="btnAdd" type="button" class="btn" data-toggle="modal" data-target="#addGroup">
                <i class="fas fa-plus mr-2"></i>
                <a href="#">Crear Grupo</a>
            </button>
            <div id="addGroups">
                <div class="modal fade" id="addGroup" tabindex="-1" aria-labelledby="exampleAddGroups" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h5>Crear grupo</h5>
                                <form method="POST" action="groups" class="text-center">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <p>Nombre del grupo</p>
                                        <input name='nameGroup' id="nameGroup" type="text" class="form-control"
                                            placeholder="Escribe el nombre del grupo..." onkeyup="return nameGroups(this);"
                                            required>
                                    </div>

                                    <div class="form-group">
                                        <p>Programa</p>
                                        <select name='selectProgram' id="selectProgram" class="form-control mb-4"
                                            onchange="selectPrograms(this.value);" required>
                                            <option value="0">Elige algún programa...</option>
                                            <option value="1">Licenciatura en derecho</option>
                                            <option value="2">Maestría en derecho</option>
                                            <option value="3">Doctorado en derecho</option>
                                            <option value="4">Jornada de titulacion licenciatura</option>
                                            <option value="5">Jornada de titulacion doctorado</option>
                                            <option value="6">Jornada de titulacion maestria</option>
                                        </select>
                                    </div>
                                    <div id="loadingSpinner" class="spinner-border mt-3" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <div id="quarter" class="form-group">
                                        <p id="quarterP">Cuatrimestre</p>
                                        <select name="selectSemester" id="selectQuarter" class="form-control mb-4"
                                            onchange="selectQuarters(this.value);" required>
                                            <option value="">Selecciona algún Cuatrimestre...</option>
                                        </select>
                                    </div>

                                    <div id="loadingSpinner2" class="spinner-border mt-3" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>

                                    <div id="matter" class="form-group">
                                        <p>Materia</p>
                                        <select name="selectMatter[]" id="selectMatter" multiple class="form-control"
                                            onchange="selectMatters(this.value);" required>
                                            <option value="1">Materia 1</option>
                                            <option value="2">Materia 2</option>
                                            <option value="3">Materia 3</option>
                                            <option value="4">Materia 4</option>
                                            <option value="5">Materia 5</option>
                                            <option value="6">Materia 6</option>
                                        </select>
                                    </div>

                                    <div class="mt-5 mb-3">
                                        <button id="cancelGroup" type="button" class="btn"
                                            data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn">Crear</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="class">
            @foreach ($listGroups as $listGroup)
                <div class="card">
                    <div class="information">
                        <div>
                            <h5 class="mb-0">{{ $listGroup->name }}</h5>
                            <p class="mb-0">{{ $listGroup->nameProgram }}</p>
                            <p class="mb-0">{{ $listGroup->semesterNumber }}</p>
                        </div>
                        <div id="optionGroups">
                            <div class="btn-group">
                                <button type="button" class="btn"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span><i class="fas fa-ellipsis-v"></i></span>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item viewGroup" href="#">Ver</a>
                                    <a class="dropdown-item editGroup" href="#">Editar</a>
                                    <a onclick="return deleteGroup({{$listGroup->id}})" class="dropdown-item deleteGroup" href="#">Eliminar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space">
                        <p class="ml-2 mt-2 mb-2">Alumnos inscritos: 0</p>
                        <p class="ml-2">Maestros inscritos: 0</p>
                    </div>
                </div>
            @endforeach
        </div>


        {{-- MODAL EDITAR GRUPO --}}
        <div class="modal fade" id="editGroups" tabindex="-1" aria-labelledby="exampleEditGroups" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <h5 class="text-center text-bold">Crear grupo</h5>
                        <form method="POST" action="groups" class="text-center ml-4 mr-4 ">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <p>Nombre del grupo</p>
                                <input name='nameGroup' id="nameGroup" type="text" class="form-control"
                                    placeholder="Escribe el nombre del grupo..." onkeyup="return nameGroups(this);"
                                    required>
                            </div>

                            <div class="form-group">
                                <p>Programa</p>
                                <select name='selectProgram' id="selectProgram" class="form-control mb-4"
                                    onchange="selectPrograms(this.value);" required>
                                    <option value="0">Elige algún programa...</option>
                                    <option value="1">Licenciatura en derecho</option>
                                    <option value="2">Maestría en derecho</option>
                                    <option value="3">Doctorado en derecho</option>
                                    <option value="4">Jornada de titulacion licenciatura</option>
                                    <option value="5">Jornada de titulacion doctorado</option>
                                    <option value="6">Jornada de titulacion maestria</option>
                                </select>
                            </div>

                            <div id="quarter" class="form-group">
                                <p id="quarterP">Cuatrimestre</p>
                                <select name="selectSemester" id="selectQuarter" class="form-control mb-4"
                                    onchange="selectQuarters(this.value);" required>
                                    <option value="">Selecciona algún Cuatrimestre...</option>
                                </select>
                            </div>

                            <div id="matter" class="form-group">
                                <p>Materia</p>
                                <select name="selectMatter[]" id="selectMatter" multiple class="form-control"
                                    onchange="selectMatters(this.value);" required>
                                    <option value="1">Materia 1</option>
                                    <option value="2">Materia 2</option>
                                    <option value="3">Materia 3</option>
                                    <option value="4">Materia 4</option>
                                    <option value="5">Materia 5</option>
                                    <option value="6">Materia 6</option>
                                </select>
                            </div>

                            <div class="mt-5 mb-3">
                                <button id="cancelGroup" type="button" class="btn"
                                    data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn">Crear</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
