@extends('layouts.dashboard')

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/Home/home.js') }}" ></script> 
<script src="{{ asset('js/Library/filterLibrary.js') }}" ></script> 
@include('popper::assets')
@endsection

@section('home')
    <div id="accountsAdmin" class="library w-100 h-100">
        <div class="container" style="width: 90%; height: 60%; margin-top: 30px">
                <div id="AddPayment" class="d-flex justify-content-between">
                    <button id="conciliate" class="btn" type="button">
                        <i class="far fa-file-excel"></i>
                        Descargar conciliación
                    </button>  
                </div>

        
            <div class="books h-100 w-100 text-center">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td>Nombre del Alumno</td>
                            <td>Concepto Pago</td>
                            <td>Monto pago</td>
                            <td>Fecha pago</td>
                            <td>Status</td>
                        </tr>
                    </thead>
                            
                    <tbody>
                        @foreach ($listAcount as $item)
                                <tr>
                                        {{-- <td>{{$loop->iteration}}</td> --}}
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->payConcept}}</td>
                                        <td>{{$item->paymentAmount}}</td>
                                        <td>{{$item->payDay}}</td>
                                        <td>{{$item->status}}</td>
                                </tr>
                        @endforeach
                </tbody>
                </table>
            </div>
        </div>

        
    </div>
@endsection