@extends('layouts.dashboard')

@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('js/Home/home.js') }}" ></script> 
<script src="{{ asset('js/Library/filterLibrary.js') }}" ></script> 
@include('popper::assets')
@endsection

@section('home')
<div class=" library h-100 w-100 bg-white pl-5">
    {{--  /*Encabezado libreria*/  --}}
    <div class="everywhere w-100 d-flex">
        <div class="sectionlibrary ">
            <div class="d-flex align-items-center">
                <i class="fas fa-book"></i>
                <p class="mr-4">Libro</p>
            </div>
            <div class="d-flex align-items-center">
                <i class="fas fa-book"></i>
                <p class="mr-4">Revista</p>
            </div>
            <div class="d-flex align-items-center">
                <i class="fas fa-book"></i>
                <p class="mr-4">Tesis</p>
            </div>
        </div>
        <div class="logolibrary align-self-center">
            <img src="{{ asset('images/BIBLIO.png')}}" alt="Logo">
        </div>
    </div>

    {{--  Contenedor de la tabla con libros  --}}
    <div id="SearchBook" class="h-50">
        <form id="sendPetititon" style="display:flex" class="form-group">
				{{csrf_field()}}
                <div class="containeInputFilter">
                    <input value="{{$filter}}" id = 'searchLibrary' name="find" class="form-control bg-white" type="search" placeholder="Escribe el titulo del libro..." aria-label="Search">
                </div>
                <div class="containeInputFilter">
                @if($showClear)
                    <button id="clearButton" {{ Popper::arrow()->pop('Eliminar') }}  type="button" class="btn btnSearch"><i class="fas fa-trash-alt"></i></button>
                @endif	 
                    <button {{ Popper::arrow()->pop('Buscar') }} id='applyFilter' class="btn btnSearch" type="submit"><i class="fas fa-search"></i></button>    
                </div>
        </form>
        <div class="books h-100">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <td>ID</td>
                        <td>Nombre del libro</td>
                        <td>Autor del libro</td>
                        <td>Ver</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($listBooks as $book)
                    <tr >
                        <td>{{ $book->id }}</td>
                        <td>{{ $book->bookName }}</td>
                        <td>{{ $book->bookAuthor }}</td>
                        <td>
                             <a target="_blank" href="{{asset('bookPdf/'.$book->bookFile)}}" {{ Popper::arrow()->pop('ver') }}><i class="fas fa-eye"></i></a> 
                              {{-- <iframe target="_blank" width="400" height="400" src="{{asset('bookPdf/'.$book->bookFile)}}" frameborder="0"></iframe>  --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="page">
            {{ $listBooks->appends(['find'=> $filter])->links('pagination::bootstrap-4') }}
            {{--  {{ $flights->links(‘vendor.pagination.simplePaginate’) }}  --}}
        </div>

    </div>
</div>
@endsection


