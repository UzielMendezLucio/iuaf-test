<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Matricula</th>
            <th>No Autorización</th>
            <th>Metodo de pago</th>
            <th>Concepto de pago</th>
        </tr>
    </thead>
    @foreach ($accountStatus as $item)
    <tbody>
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->nameComplete}}</td>
            <td>{{$item->registration}}</td>
            <td>{{$item->noAutorization}}</td>
            <td>{{$item->paymentAmmount}}</td>
            <td>{{$item->paymentMethodx}}</td>
        </tr>   
    @endforeach
    </tbody>
</table>