<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Role;
 use App\Models\Permission;

// Route::get('/', function () {
//     return view('Auth.Login');
// });

//Solamente pasaran los usuarios que no esten autenticados
Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'showLoginView'])->middleware('guest') ;

Route::get('/classes', 'App\Http\Controllers\ClassesController@index');

Route::get('/academic', 'App\Http\Controllers\AcademicYearController@index');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


//Ruta de pruebas no borrar

//Prueba de sweetAlert 

Route::get('redirect', function () {
    alert()->info('Exito','try');
    return redirect('/home');
});

///Prueba de roles
Route::get('/prueba', function(){

    //1. Creacion de Roles
//  return Role::create([
//     'name'=> 'Admin',
//     'slug'=> 'admin',
//     'description'=> 'administrator',
//     'full-access'=> 'yes'
// ]);

// return Role::create([
//     'name'=> 'Student',
//     'slug'=> 'student',
//     'description'=> 'student',
//     'full-access'=> 'no'
// ]);

// return Role::create([
//     'name'=> 'Teacher',
//     'slug'=> 'teacher',
//     'description'=> 'teacher',
//     'full-access'=> 'no'
// ]);

//2. Asignar el rol 

//Limpio
//$user =   User::find(1);
//$user ->roles()->sync(1);
//return $user->roles;  

// 3. Se crea el permiso 
//  return Permission::create([
//      'name'=> 'Create product',
//      //Nombre del Modelo + el nombre del metodo asociado al controlador
//      'slug'=> 'product.create',
//      'description'=> 'Pequeña descripcion acerca del permiso',
//  ]);

// 4. Asignar un Permiso

// Localizamos el rol
//  $role =   Role::find(1);

//  $role->permission()->sync(1);
//  return $role->permissions;
  
})->name('test');
// Route::get('/library', 'App\Http\Controllers\BookLibraryController@index');
Route::resource('library', App\Http\Controllers\BookLibraryController::class);

Route::get('accounts/export/{type}/params/{param}/secondParms/{secondParam}', 'App\Http\Controllers\AccountStatusController@export')->name('accounts.exports');

Route::resource('accounts', App\Http\Controllers\AccountStatusController::class);

Route::view('/accountsAdmin', 'accountsAdmin');
//Importarlo mas tarde
//Route::view('/studentMaterial', 'studentMaterial');

Route::resource('bankMatters', App\Http\Controllers\SubjectsController::class );
Route::patch('bankMatters/disabled/{id}', 'App\Http\Controllers\SubjectsController@disableGroup');

Route::resource('groups', App\Http\Controllers\GroupController::class);
Route::patch('groups/disabled/{id}', 'App\Http\Controllers\GroupController@disableGroup');

//Export

//Export xls
// Route::get('account/export/', App\Http\Controllers\AccountStatusController::class);

//Route::get('accounts/export/', [App\Http\Controllers\AccountStatusController::class, 'export'])->name('exports');


// Route::get('accounts/export', function () {
//     [App\Http\Controllers\AccountStatusController::class, 'export'];
//     return redirect('/');
// });

// Route::get('accounts/exporsssst', function () {
//     alert()->info('Exito','try');
//     return redirect('/home');
// });

