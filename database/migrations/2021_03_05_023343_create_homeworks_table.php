<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homeworks', function (Blueprint $table) {
            $table->increments('id');
            $table->text('classId');
            $table->text('sectionId');
            $table->integer('subjectId');
            $table->integer('teacherId');
            $table->string('homeworkTitle',250);
            $table->longText('homeworkDescription');
            $table->string('homeworkFile',250);
            $table->string('homeworkDate',250);
            $table->integer('homeworkSubmissionDate');
            $table->integer('homeworkEvaluationDate');
            $table->longText('studentsCompleted');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homeworks');
    }
}
