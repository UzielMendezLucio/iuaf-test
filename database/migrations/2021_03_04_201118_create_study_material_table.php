<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudyMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_material', function (Blueprint $table) {
            $table->increments('id');
            $table->text('class_id');
            $table->text('sectionId');
            $table->integer('subject_id');
            $table->integer('teacher_id');
            $table->string('material_title', 250);
            $table->text('material_description');
            $table->string('material_file', 250);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_material');
    }
}
