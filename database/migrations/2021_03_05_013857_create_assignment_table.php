<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('classId');
            $table->text('sectionId');
            $table->integer('subjectId');
            $table->integer('teacherId');
            $table->string('AssingTitle',250);
            $table->text('AssingDescription',250);
            $table->string('AssingFile',250);
            $table->string('AssingDeadLine',250);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
