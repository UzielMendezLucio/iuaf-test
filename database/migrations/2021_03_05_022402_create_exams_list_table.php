<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams_list', function (Blueprint $table) {
            $table->increments('id');
            $table->string('examTitle',250);
            $table->text('examDescription',250);
            $table->string('examDate',250);
            $table->integer('examEndDate');
            $table->text('examClasses');
            $table->text('examMarksheetColumns');
            $table->integer('examAcYear');
            $table->text('examSchedule');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams_list');
    }
}
