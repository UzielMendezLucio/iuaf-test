<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('username',250);
            $table->string('email',250);
            $table->string('password',100);
            $table->string('remember_token',250)->nullable();
            $table->string('fullName',250)->nullable();
            $table->string('role',10)->nullable();
            $table->integer('role_perm')->nullable();
            $table->integer('department')->nullable();
            $table->integer('designation')->nullable();
            $table->integer('activated')->nullable();
            $table->string('studentRollId',250)->nullable();
            $table->string('admission_number',250)->nullable();
            $table->integer('admission_date')->nullable();
            $table->integer('std_category')->nullable();
            $table->text('auth_session')->nullable();
            $table->integer('birthday')->nullable();
            $table->string('gender', 10)->nullable();
            $table->text('address')->nullable();
            $table->string('phoneNo',250)->nullable();
            $table->string('mobileNo',250)->nullable();
            $table->integer('studentAcademicYear')->nullable();
            $table->integer('studentClass')->nullable();
            $table->integer('studentSection')->nullable();
            $table->string('religion',250)->nullable();
            $table->string('parentProfession',250)->nullable();
            $table->text('parentOf')->nullable();
            $table->string('photo',250)->nullable();
            $table->text('isLeaderBoard')->nullable();
            $table->string('restoreUniqId',250)->nullable();
            $table->integer('transport')->nullable();
            $table->integer('transport_vehicle')->nullable();
            $table->integer('hostel')->nullable();
            $table->longtext('medical')->nullable();
            $table->string('user_position',250)->nullable();
            $table->integer('defLang')->nullable();
            $table->string('defTheme',20)->nullable();
            $table->string('salary_type',250)->nullable();
            $table->integer('salary_base_id')->nullable();
            $table->text('comVia')->nullable();
            $table->text('father_info')->nullable();
            $table->text('mother_info')->nullable();
            $table->integer('biometric_id')->nullable();
            $table->string('library_id',250)->nullable();
            $table->integer('account_active')->nullable();
            $table->date('update_at')->nullable();
            $table->string('customPermissionsType',10)->nullable();
            $table->text('customPermissions')->nullable();
            $table->longText('firebase_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
