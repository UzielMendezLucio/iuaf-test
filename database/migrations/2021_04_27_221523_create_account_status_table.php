<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nameComplete',50);
            $table->string('registration',100);
            $table->string('noAutorization',50);
            $table->integer('paymentAmount');
            $table->string('paymentMethod',50);
            $table->string('payConcept',100);
            $table->string('proofOfPayment',50);
            $table->date('payDay');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_status');
    }
}
