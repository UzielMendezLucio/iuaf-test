<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('classId');
            $table->integer('sectionId');
            $table->integer('subjectId');
            $table->string('dayOfWeek',10);
            $table->integer('teacherId');
            $table->string('startTime',20);
            $table->string('endTime',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_schedule');
    }
}
