<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookLibraryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_library', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bookName',250);
            $table->text('bookDescription');
            $table->string('bookISBN',250);
            $table->string('bookAuthor',250);
            $table->string('bookPublisher',250);
            $table->string('bookType',250);
            $table->string('bookPrice',250);
            $table->string('bookFile',250);
            $table->integer('bookState');
            $table->integer('bookQuantity');
            $table->text('bookShelf',250);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_library');
    }
}
