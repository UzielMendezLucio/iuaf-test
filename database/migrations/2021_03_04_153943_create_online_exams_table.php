<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlineExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_exams', function (Blueprint $table) {
            $table->id();
            $table->string('examTitle', 250);
            $table->text('examDescription');
            $table->string('examClass', 250);
            $table->text('sectionId');
            $table->integer('examTeacher');
            $table->integer('examSubjects');
            $table->string('examDate', 250);
            $table->integer('exAcYear');
            $table->string('ExamEndDate', 250);
            $table->integer('examTimeMinutes');
            $table->integer('examDegreeSuccess');
            $table->integer('ExamShowGrade');
            $table->integer('random_question');
            $table->text('examQuestion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_exams');
    }
}
