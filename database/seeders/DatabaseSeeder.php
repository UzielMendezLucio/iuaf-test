<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
// Traernos Role y permisos
use App\Models\Role;
use App\Models\Permission;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Vaciamos las tablas 
        //No tienen modelo
        //Se deshabilita los foreign Key
        DB::statement('SET foreign_key_checks=0');
        DB::table('role_user')->truncate();
        DB::table('permission_role')->truncate();    
        Permission::truncate();
        Role::truncate();
        //Se vuelve habilitar
        DB::statement('SET foreign_key_checks=1');



        // \App\Models\User::factory(10)->create();
        $this->call(RolesSeeder::class);

        //Una de las formass para verificar que esta correctamente
        //dev
        //php artisan migrate:fresh --seed
        //prod
        //php artisan migrate --seed o db:seed


        //User Admin
        $userAdmin = User::where('email','admin@gmail.com')->first();
        if($userAdmin){
            $userAdmin->delete();
        }

        $userAdmin = User::create([
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('12345678'),
        ]);

        //Role Admin
         $roleAdmin = Role::create([
            'name'=> 'Admin',
            'slug'=> 'admin',
            'description'=> 'Aministrator',
            'full-access'=> 'yes'
        ]);

        //Role Registered User
        $roleUser = Role::create([
            'name'=> 'Student User',
            'slug'=> 'registeredstudent',
            'description'=> 'Student User',
            'full-access'=> 'no'
        ]);


        //La relacion entre dos tablas
        $userAdmin->roles()->sync([$roleAdmin->id]);


        // Creacion del Permiso
        $permissionList = [];
         $permission =  Permission::create([
              'name'=> 'Permiso para acceder a la libreria ejemplo ',
              //Nombre del Modelo + el nombre del metodo asociado al controlador
              'slug'=> 'libraryExample.index',
              'description'=> 'Este usuario puede acceder a la vista libraryExample al metodo index',
          ]);
          
          //Agrego su id que acabo de crear
          $permissionList[]= $permission->id;
          
          $permission =  Permission::create([
            'name'=> 'Permiso para crear a la libreria ejemplo ',
            //Nombre del Modelo + el nombre del metodo asociado al controlador
            'slug'=> 'libraryExample.create',
            'description'=> 'Este usuario puede agregar un libro  a la vista libraryExample al metodo crear',
        ]);

          //Agrego su id que acabo de crear
          $permissionList[]= $permission->id;

        //La relacion entre dos tablas permission_role
        $roleAdmin->permissions()->sync($permissionList);


    }
}
