<?php

namespace App\Http\Controllers;

use App\Models\AccountStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

//use App\Exports\ReportAccountExport;
use App\Exports\AccountReport;

use function PHPUnit\Framework\isEmpty;

class AccountStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $id = Auth::id();
        $roleActual = DB::table('role_user')->where('user_id', $id)->first();
        $listAcount = AccountStatus::paginate(5);
        $showClearFilter = false;

        $getValue = null;
        $getValue2 = date('YYYY-mm-dd');
        $options = $request->get('options');
        $filterName = $request->get('searchFilterName');
        $filterFrom = $request->get('filterFrom');
        $filterTo = $request->get('filterTo');
        $filterAuto = $request->get('filterAuto');

        if ($options == 'name') {
            $getValue = $filterName;
        } else if ($options == 'date') {
            $getValue = $filterFrom;
            $getValue2 = $filterTo;
        } else if ($options == 'auto') {
            $getValue = $filterAuto;
        }

        $listAcount = DB::table('account_status')
            ->where('nameComplete', 'LIKE', '%' . $getValue . '%')
            ->orWhereBetween('payDay', [$getValue, $getValue2])
            ->orWhere('noAutorization', 'LIKE', '%' . $getValue . '%')
            ->paginate(5);

            if(count($listAcount)>0){
                $showClearFilter = true;
            }    

        return view('accounts', compact('listAcount', 'roleActual', 'id', 'showClearFilter', 'filterName', 'filterFrom', 'filterTo', 'filterAuto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestAccount = request()->except('_token');
        $numberAuthBank = $requestAccount['noAutorization'];

        $isExist = DB::table('account_status')->where('noAutorization', $numberAuthBank)->first();
        if (empty($isExist)) {
            AccountStatus::insert([
                // 'id' => $findMaterial->idMaterial, 
                'nameComplete' => $requestAccount['nameComplete'],
                'registration' => $requestAccount['registrationUser'],
                'noAutorization' => $requestAccount['noAutorization'],
                'paymentAmount' => $requestAccount['paymentAmount'],
                'paymentMethod' => $requestAccount['paymentMethod'],
                'payConcept' => $requestAccount['payConcept'],
                'proofOfPayment' => $requestAccount['proofOfPayment'],
                'payDay' => $requestAccount['payDay'],
                'status' => false,
            ]);
            alert()->success('Éxito', 'Se agregó correctamente');
        } else {
            alert()->error('Upss', 'Lo sentimos, ya se registró el número de autorización');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AccountStatus  $accountStatus
     * @return \Illuminate\Http\Response
     */
    public function show(AccountStatus $accountStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AccountStatus  $accountStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(AccountStatus $accountStatus)
    {
        //
    }

    public function export($type, $param, $optionalParam)
    {
        if ($type == 'date') {
            $paramSecondFilter = $optionalParam;
        } else {
            $paramSecondFilter = null;
        }
        return (new AccountReport($type, $param, $optionalParam))->download('iuafCuentas.xlsx');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AccountStatus  $accountStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountStatus $accountStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AccountStatus  $accountStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountStatus $accountStatus)
    {
        //
    }
}
