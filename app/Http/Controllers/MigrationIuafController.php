<?php

namespace App\Http\Controllers;

use App\Models\MigrationIuaf;
use Illuminate\Http\Request;

class MigrationIuafController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MigrationIuaf  $migrationIuaf
     * @return \Illuminate\Http\Response
     */
    public function show(MigrationIuaf $migrationIuaf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MigrationIuaf  $migrationIuaf
     * @return \Illuminate\Http\Response
     */
    public function edit(MigrationIuaf $migrationIuaf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MigrationIuaf  $migrationIuaf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MigrationIuaf $migrationIuaf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MigrationIuaf  $migrationIuaf
     * @return \Illuminate\Http\Response
     */
    public function destroy(MigrationIuaf $migrationIuaf)
    {
        //
    }
}
