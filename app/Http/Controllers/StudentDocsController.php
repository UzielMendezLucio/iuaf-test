<?php

namespace App\Http\Controllers;

use App\Models\StudentDocs;
use Illuminate\Http\Request;

class StudentDocsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StudentDocs  $studentDocs
     * @return \Illuminate\Http\Response
     */
    public function show(StudentDocs $studentDocs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StudentDocs  $studentDocs
     * @return \Illuminate\Http\Response
     */
    public function edit(StudentDocs $studentDocs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StudentDocs  $studentDocs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StudentDocs $studentDocs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StudentDocs  $studentDocs
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentDocs $studentDocs)
    {
        //
    }
}
