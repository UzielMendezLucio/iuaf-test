<?php

namespace App\Http\Controllers;

use App\Models\online_exams;
use Illuminate\Http\Request;

class OnlineExamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\online_exams  $online_exams
     * @return \Illuminate\Http\Response
     */
    public function show(online_exams $online_exams)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\online_exams  $online_exams
     * @return \Illuminate\Http\Response
     */
    public function edit(online_exams $online_exams)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\online_exams  $online_exams
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, online_exams $online_exams)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\online_exams  $online_exams
     * @return \Illuminate\Http\Response
     */
    public function destroy(online_exams $online_exams)
    {
        //
    }
}
