<?php

namespace App\Http\Controllers;

use App\Models\AcademicYear;
use Illuminate\Http\Request;

class AcademicYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          //$article['products'] = Classes::paginate(5);
          $article = AcademicYear::all();
        return view('Academic.index',compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\academic_year  $academic_year
     * @return \Illuminate\Http\Response
     */
    public function show(academic_year $academic_year)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\academic_year  $academic_year
     * @return \Illuminate\Http\Response
     */
    public function edit(academic_year $academic_year)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\academic_year  $academic_year
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, academic_year $academic_year)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\academic_year  $academic_year
     * @return \Illuminate\Http\Response
     */
    public function destroy(academic_year $academic_year)
    {
        //
    }
}
