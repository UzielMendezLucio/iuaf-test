<?php

namespace App\Http\Controllers;

use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('find');
        $showClear = false;
         $listGroups = DB::table('groups')
         ->join('programs', 'programs.id', '=', 'groups.idProgram')
         ->select('groups.id','groups.name', 'groups.semesterNumber','programs.nameProgram as nameProgram')
         ->where('groups.status' ,'=', 1)
         ->paginate(10);

        if($filter== ''){
         return view('studentMaterial', compact('listGroups', 'filter','showClear'));
        }
        //Filter
        $getDataLibrary = DB::table('groups')
        ->join('programs', 'programs.id', '=', 'groups.idProgram')
        ->select('groups.id','groups.name', 'groups.semesterNumber','programs.nameProgram as nameProgram')
        ->where('groups.status' ,'=', 1)
        ->where('groups.name' ,'LIKE', '%'.$filter.'%')
        ->orWhere('groups.status' ,'=', 1)
        ->where('programs.nameProgram' ,'LIKE', '%'.$filter.'%')
        ->paginate(10);

        if(count($getDataLibrary)> 0){
            $listGroups = $getDataLibrary;
            $showClear = true; 
         }
        return view('studentMaterial', compact('listGroups', 'filter','showClear'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestGroup = request()->except('_token');

        $requestAccount = request()->except('_token');
        $checkName = $requestAccount['nameGroup'];
        $isExist = DB::table('groups')->where('name', $checkName)->first();
        if (empty($isExist)) {
            
            $newGroup  = Group::insert([
                'name' => $requestAccount['nameGroup'],
                'semesterNumber' => $requestAccount['selectSemester'],
                'idProgram' => $requestAccount['selectProgram'],
                'status' => true
            ]);
        
            if($newGroup == 1  ){
                //Obtengo el ultimo registro 
                $getLastId = Group::latest('id')->first();
            for ($i=0; $i < count($requestAccount['selectMatter']) ; $i++) { 
                DB::table('groups_courses')->insert(
                    [ 
                    'idGroup' => $getLastId->id,
                    'idSubject' => $requestAccount['selectMatter'][$i],
                    ]
                );
            }
            }
        alert()->success('Éxito', 'Se agregó el grupo correctamente');
            
        } else {
            alert()->error('Upss', 'Lo sentimos, este grupo ya se registró anteriormente');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($groupId)
    {
        if($groupId != null ||$groupId != 0) {
            $result = Group::select(
                'groups.id',
                'groups.name',
                'gorups.semesterNumber',
                'programs.namePrograms')
                ->join('programs', 'programs.id', '=', 'groups.idProgram')
                ->where('id', '=',$groupId)
                ->first();

            return $result;
        }       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $requestGroup = request()->except('_token');

//        $result = Group::where('id', '=',$requestGroup['groupId'])->update(['status' => "0"]);
  //      return $result;
        // if($result == 0){
        //     return alert()->error('Upss', 'Lo sentimos, no se pudo eliminar el grupo corrctamente');
        // }
        // else{
        //     return  alert()->success('Éxito', 'Se removió el grupo correctamente');
        // }
    }
    public function disableGroup(Request $request)
    {
        $requestGroup = request()->except('_token');

        $result = Group::where('id', '=',$requestGroup['groupId'])->update(['status' => "0"]);
        return $result;
    }
    


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        //
    }
}
