<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $table = 'permissions';
    //Empieza el codigo de Permisos
    protected $fillable = [
        'name',
        'slug',
        'description'
    ];

    public function roles(){
        // un usuario puede tener muchos permisos
       return  $this->belongsToMany('App\Models\Role')->withTimesTamps();
    }
}
