<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

 use App\Models\Role;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    //protected $table = 'users_iuaf';
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = [
        'username',
        'email',
        'password',
        'role'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    //Roles
    public function roles(){

        // un usuario puede tener muchos permisos
       return  $this->belongsToMany('App\Models\Role')->withTimesTamps();
    }
}
