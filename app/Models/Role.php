<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // use HasFactory;
 //   protected $table = 'roles';
    //Empieza el codigo de Roles
    protected $fillable = [
        'name',
        'slug',
        'description',
        'full-access'
    ];

    public function users(){

        // un usuario puede tener muchos permisos
        return  $this->belongsToMany('App\Models\User')->withTimesTamps();
    }
    public function permissions(){

        // un usuario puede tener muchos permisos
       return  $this->belongsToMany('App\Models\Permission')->withTimesTamps();
    }

}

