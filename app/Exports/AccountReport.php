<?php

namespace App\Exports;

use App\Models\AccountStatus;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Support\Facades\DB;

//class AccountReport implements FromCollection
class AccountReport implements FromQuery
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function __construct(string $type,  $params = null, $paramsSecond = null ){
      $this->param = $params;
      $this->paramSecondary = $paramsSecond;
      $this->type = $type;
    }
    
    public function collection()
     {
         return AccountStatus::all();
     }

       public function view(): View
       {
          return view('exports.AccountStatus', [
              'accountStatus' => AccountStatus::all()
         ]);
      }
    public function query()
     {
      if($this->type == 'all'){
        return AccountStatus::query();
      }else if($this->type == 'date'){
        return AccountStatus::query()->whereBetween('payDay', [$this->param,$this->paramSecondary ]);
       }
       else if($this->type == 'name'){
        return AccountStatus::query()->where('nameComplete' ,'LIKE', '%'.$this->param.'%');
       }
       else if($this->type == 'auto'){
        return AccountStatus::query()->where('noAutorization' ,'LIKE', '%'.$this->param.'%');
       }
     
     }
}
