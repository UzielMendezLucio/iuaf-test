<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RegisteredRoleEvent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    //Evento se utuiliza al momento de crear un usuario 
    public function handle(Registered $event)
    {
      //  dd($event);
        //Asigno el rol especifico 
        $event->user->roles()->sync([2]);
    }
}
